package main

import (
	"fmt"
	"os"
)

func main() {
	if len(os.Args) != 3 {
		fmt.Println("Usage:\n interface hostname")
		os.Exit(1)
	}
}
